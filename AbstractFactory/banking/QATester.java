package AbstractFactory.banking;

import AbstractFactory.Tester;

public class QATester implements Tester {
    @Override
    public void testCode() {
        System.out.println("QA tester tests AbstractFactory.banking code...");
    }
}
