package Decorator;

interface Developer {
    String makeJob();
}
