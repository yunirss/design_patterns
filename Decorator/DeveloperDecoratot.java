package Decorator;

class DeveloperDecoratot implements Developer{
    Developer developer;

    public DeveloperDecoratot(Developer developer) {
        this.developer = developer;
    }

    @Override
    public String makeJob() {
        return developer.makeJob();
    }
}
