package Factory;

import java.util.Scanner;

class Program {
    public static void main(String[] args) {
        System.out.println("Enter specialty : ");
        Scanner scanner = new Scanner(System.in);
        String console = scanner.nextLine();
        DeveloperFactory developerFactory = createDeveloperBySpecialty(console);
        Developer developer = developerFactory.createDeveloper();

        developer.writeCode();
    }

    private static DeveloperFactory createDeveloperBySpecialty(String specialty) {
        if (specialty.equalsIgnoreCase("java")) {
            return new JavaDeveloperFactory();
        } else if (specialty.equalsIgnoreCase("php")) {
            return new PhpDeveloperFactory();
        } else if (specialty.equalsIgnoreCase("c++")) {
            return new CppDeveloperFactory();
        } else {
            throw new RuntimeException(specialty + " is unknown specialty");
        }
    }
}
