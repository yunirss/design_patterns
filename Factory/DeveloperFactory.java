package Factory;

interface DeveloperFactory {
     Developer createDeveloper();
}
