package Factory;

interface Developer {
    void writeCode();
}
