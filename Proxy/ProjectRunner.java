package Proxy;

class ProjectRunner {
    public static void main(String[] args) {
        Project project = new ProxyProject("https://gitlab.com/yunirss/design_patterns");
        project.run();
    }
}
